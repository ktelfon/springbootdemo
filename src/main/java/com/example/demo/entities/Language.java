package com.example.demo.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Language {
    @Id
    private Long id;

    @Override
    public String toString() {
        return "Language{" +
                "id=" + id +
                '}';
    }
}
