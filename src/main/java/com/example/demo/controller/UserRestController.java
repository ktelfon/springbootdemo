package com.example.demo.controller;

import com.example.demo.entities.User;
import com.example.demo.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("users")
public class UserRestController {

    private UserRepo userRepo;

    @Autowired
    public UserRestController(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @GetMapping("/")
    @Secured("ROLE_VISITOR")
    public List<User> getAll() {
        return userRepo.findAll();
    }

    @GetMapping("/{id}")
    public User find(@PathVariable("id") long id) {
        return userRepo.findById(id).get();
    }

    @PostMapping("/")
    public User posting(@RequestBody User user) {
        return userRepo.save(user);
    }

    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") long id) {
        userRepo.deleteById(id);
        return "Deleted";
    }

    @PutMapping("/")
    public User update(@RequestBody User user) {

        List<User> users = userRepo.findByUsername("name");
        for(User userFromDb : users ){
            userFromDb.setUsername("SOME NEW NAME");
        }
        userRepo.saveAll(users);

        if (userRepo.findById(user.getId()).isEmpty()) {
            return null;
        }
        return userRepo.save(user);
    }

    @GetMapping("/count")
    public long count(){
        return userRepo.count();
    }
}
