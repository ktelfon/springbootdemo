package com.example.demo.controller;

import com.example.demo.entities.Role;
import com.example.demo.entities.User;
import com.example.demo.repositories.RoleRepo;
import com.example.demo.repositories.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;
import java.util.Set;

@Controller
public class UserController {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private RoleRepo roleRepo;

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute(
                "users",
                userRepo.findAll());

        model.addAttribute(
                "currentUser",
                SecurityContextHolder.getContext().getAuthentication().getName());
        return "index";
    }

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @GetMapping("/edit")
    public String edit(Model model) {
        model.addAttribute(
                "user",
                new User());
        return "signup";
    }


    @PostMapping("/adduser")
    public String save(User user, Model model) {
        Role role_user = roleRepo.save(new Role("ROLE_USER"));
        user.setRoles(Set.of(role_user));
        userRepo.save(user);
        model.addAttribute("saveMsg", "New User saved.");
        return "redirect:/";
    }
}
