package com.example.demo.dto;

import com.example.demo.entities.User;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserResp {
    private User user;
    private String error;
}
